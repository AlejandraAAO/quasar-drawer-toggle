
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Master.vue') },
      { path: '/clientes', component: () => import('pages/mod-Maestros/Clientes.vue') },
      { path: '/usuarios', component: () => import('pages/mod-Maestros/Usuarios.vue') },
      { path: '/sedes', component: () => import('pages/mod-Maestros/Sedes.vue') },
      { path: '/productos', component: () => import('pages/mod-Maestros/Productos.vue') },
      { path: '/rutas', component: () => import('pages/mod-Maestros/Rutas.vue') },
      { path: '/carteras', component: () => import('pages/mod-Maestros/Carteras.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
